
const Table = ({
    books = [],
    onChange
}) => {

    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Harga</th>
                    </tr>
                </thead>
                <tbody>
                    {books.map((data, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{data.name}</td>
                            <td>{data.harga}</td>
                        </tr>
                    ))}
                    </tbody>
            </table>
            
        </div>
    )
}

export default Table
