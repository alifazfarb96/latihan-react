import './App.css';
import React, { useState} from 'react';
import Table from './Table';

function App() {

  const [name, setName] = useState("");
  const [harga, setHarga] = useState("");
  const [books, setBooks] = useState([]);

  const handleSubmit = () => {
    console.log(name, harga)
    const listBook = {name,harga}
    if (name && harga) {
      setBooks([...books,listBook])
      setName("")
      setHarga("")
    }
  }

  return (
    <div className="App">
      <p className="page">Table Buku</p>
      <div className="form">
        <p>Nama</p>
        <input name="name" placeholder="Nama" value={name} onChange={(e) => setName(e.target.value)} />
        <p>Harga</p>
        <input name="harga" placeholder="Harga" value={harga} onChange={(e) => setHarga(e.target.value)}/>
        <br></br>
        <button onClick={handleSubmit}>Simpan</button>
        <br></br>
        
      </div>
      
     <Table books={books} onChange={handleSubmit}/> 

    </div>

    
  );

}

export default App;